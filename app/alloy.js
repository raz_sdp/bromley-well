var XHR = require("xhr");
Alloy.Globals.xhr = new XHR();
//var root = Ti.App.deployType != "production" ? 'http://74.3.255.227/carers/' : 'http://74.3.255.228/carers/';
var root = Ti.App.deployType != "production" ? "http://35.187.95.29/bromley_well/" : "http://35.187.95.29/bromley_well/";

if (!OS_IOS) {
    Alloy.Globals.font = {
        "balooregular" : "baloo-regular",
        "baloobhaiRegular" : "baloobhai-Regular",
        "baloobhainaRegular" : "baloobhaina-Regular",
        "baloochettanRegular" : "baloochettan-Regular",
        "baloodaRegular" : "balooda-Regular",
        "baloopaajiRegular" : "baloopaaji-Regular",
        "balootammaRegular" : "balootamma-Regular",
        "balootammuduRegular" : "balootammudu-Regular",
        "balooThambiRegular" : "balooThambi-Regular",
        "latoblack" : "lato-black",
        "latobold" : "lato-bold",
        "latoheavy" : "lato-heavy",
        "latoregular" : "lato-regular",
        "latosemibold" : "latosemibold",
    };
} else {
    Alloy.Globals.font = {
        "balooregular" : "Baloo-Regular",
        "baloobhaiRegular" : "BalooBhai",
        "baloobhainaRegular" : "BalooBhaina",
        "baloochettanRegular" : "BalooChettan",
        "baloodaRegular" : "BalooDa",
        "baloopaajiRegular" : "BalooPaaji",
        "balootammaRegular" : "BalooTamma",
        "balootammuduRegular" : "BalooTammudu",
        "balooThambiRegular" : "BalooThambi",
        "latoblack" : "Lato-Black",
        "latobold" : "Lato-Bold",
        "latoheavy" : "Lato-Heavy",
        "latoregular" : "Lato-Regular",
        "latosemibold" : "Lato-Semibold",
    };
}
Alloy.Globals.root = root;
Alloy.Globals.allDataURL = root + 'notifications_users/g/';
Alloy.Globals.complaintURL = root + 'complaints/complaint_entry';
Alloy.Globals.notificationReadURL = root + '/notifications_users/notification_read';
Alloy.Globals.notificationDeleteURL = root + '/notifications_users/notification_delete';
Alloy.Globals.feedbackSubmitURL = root + 'feedbacks/feedback_submission/';
//co.uk.carersfederation.app
//com.abcoder.carerfederationtest
Alloy.Globals.getAssesmentURL = root + "assesments/get_assesment";
Alloy.Globals.postAssesmentURL = root + "users_options/user_option_insert";
Alloy.Globals.checkEnabilityURL = root + 'users/check_enability/';

Alloy.Globals.termsURL = root + "terms-of-use";
Alloy.Globals.awardsURL = root + 'feedbacks/award_info/';

Alloy.Globals.staffalerts = root + "staffalerts/get_all_alerts/";
Alloy.Globals.submit_alert = root + 'staffalerts/submit_alert';
Alloy.Globals.alertDeleteURL = root + 'staffalerts/delete_alert/';
Alloy.Globals.submit_thought = root + 'thoughts/submit_thought';


Alloy.Globals.dataSynchronize = require('downloadAllData').downloadData;
Alloy.Globals.getLocalFile = require('downloadAllData').getLocalFile;
Alloy.Globals.navWin = null;
Alloy.Globals.currentWins = [];
Alloy.Globals.isClosed = true;
var imageFile = null;
Alloy.Globals.winOpener = function(winName, closePrevWins, winParams) {
    if (OS_IOS) {
        var win = Alloy.createController(winName, winParams || {}).getView();

        if (closePrevWins === true || Alloy.Globals.navWin === null) {
            var navWin = Ti.UI.iOS.createNavigationWindow({
                window : win,
                width : Ti.Platform.displayCaps.platformWidth
            });
            navWin.left = (Alloy.Globals.navWin === null) ? 0 : -320;
            navWin.open({
                animated : false // highly effective to eliminate the black screen delay during launch!
            });
            if (!!navWin.left) {
                navWin.animate({
                    left : 0,
                    curve : Ti.UI.ANIMATION_CURVE_EASE_OUT,
                    duration : 250
                });
                Alloy.Globals.navWin.animate({
                    left : 320,
                    curve : Ti.UI.ANIMATION_CURVE_EASE_OUT,
                    duration : 300
                }, function() {
                    Alloy.Globals.navWin.close();
                    Alloy.Globals.navWin = navWin;
                });
            } else {
                Alloy.Globals.navWin = navWin;
            }
            if (Alloy.Globals.navWin !== null) {

                Alloy.Globals.currentWins = [];
            }

        } else {
            Alloy.Globals.navWin.openWindow(win);
        }

        Alloy.Globals.st(win, win.getTitle());
        Alloy.Globals.currentWins.push(win);
    } else {
        var win = Alloy.createController(winName, winParams || {}).getView();
        win.width = Ti.Platform.displayCaps.platformWidth / Ti.Platform.displayCaps.logicalDensityFactor;
        Ti.API.info(' 1 open ' + winName);
        win.open();
        Ti.API.info(' 4 open ');

        win.addEventListener('android:back', function() {
            Ti.API.info(' 5 open ');
            if (winName === 'Login') {
                return false;
            } else if (winName === 'Home') {
                return false;
            } else if (closePrevWins === true) {
                return false;
            } else {
                Alloy.Globals.b();
            }
        });
        win.addEventListener('open', function() {
            Ti.API.info(' 6 open ');
            if (closePrevWins === true) {
                _closePrevWins(win);
            } else {
                Ti.API.info(' 8 open ');
                Alloy.Globals.currentWins.push(win);
            }
        });
    }
};

Alloy.Globals.st = function(win, title) {
    win.setTitleControl(Ti.UI.createLabel({
        text : title,
        color : '#FFF',
        font : {
            fontSize : 18,
            //fontWeight : 'bold'
        }
    }));
};

Alloy.Globals.b = function(e) {

    if (Alloy.Globals.currentWins.length > 1) {
        if (OS_IOS) {
            Alloy.Globals.navWin.closeWindow(Alloy.Globals.currentWins.pop());
        } else {
            var currentWin = Alloy.Globals.currentWins.pop();
            currentWin.close();
            return false;
        }
    }
};

Alloy.Globals.isActive_data = {
    success : false,
    msg : "No internet connectivity is available, please try again later."
};
Alloy.Globals.checkUserActivation = function() {
    if (!!Alloy.Globals.xhr.isOnline()) {
        var user = Ti.App.Properties.getObject('user');
        if (!!user && user.id) {
            var url = Alloy.Globals.checkEnabilityURL + user.id + '/' + user.carerid;
            Alloy.Globals.xhr.get(url, function(responceData) {
                Ti.API.info('active ' + JSON.stringify(responceData));
                Alloy.Globals.isActive_data = responceData.data;
            }, function(errResult) {
                //alert(errResult);
                Alloy.Globals.isActive_data = {
                    success : false,
                    msg : "No internet connectivity is available, please try again later."
                };
            }, {});
        }
    } else {
        Alloy.Globals.isActive_data = {
            success : false,
            msg : "No internet connectivity is available, please try again later."
        };
    }
};
Alloy.Globals.isiPhoneX = function() {
    Ti.API.info('jyhfjjj');
    return (Ti.Platform.displayCaps.platformWidth === 375) && (Ti.Platform.displayCaps.platformHeight === 812) && (Ti.Platform.displayCaps.logicalDensityFactor === 3);
};
Ti.App.addEventListener('resumed', function(e) {
    Alloy.Globals.checkUserActivation();
});
function _closePrevWins(win) {
    // Close previously opened windows
    Ti.API.info('6:   ' + Alloy.Globals.currentWins.length);
    Ti.API.info(' close 1 ');
    for (var i = Alloy.Globals.currentWins.length - 1; i >= 0; i--) {
        Ti.API.info(' close 2');
        try {
            Ti.API.info(' close 3');
            Alloy.Globals.currentWins[i].close({
                animated : false
            });
        } catch(e) {
            Ti.API.info(' close 4');
            Ti.API.info(' Ti.API.info ' + e);
        }
    }
    // setTimeout(function() {
    Ti.API.info(' close 5');
    Alloy.Globals.currentWins = [];
    Ti.API.info(' close 6');
    Alloy.Globals.currentWins.push(win);
    Ti.API.info(' close 7');
    Ti.API.info('7:   ' + Alloy.Globals.currentWins.length);
    // }, 50);

}

//For push notification
if (OS_IOS) {
    var notification = function() {
        Ti.UI.iOS.setAppBadge(null);
        function successCallback(e) {
            Ti.API.info("successCallback: " + e.deviceToken);
            if (Titanium.Platform.model == 'Simulator') {
                Ti.App.Properties.setString('deviceToken', 'c3ae02e6b758ae28101697f53dc794f46957441151310644d9d2bb719e0fbe4a');
            } else {
                Ti.App.Properties.setString('deviceToken', e.deviceToken);
            }
        }

        function errorCallback(e) {
            Ti.API.info("Error during registration: " + e.error);
        }

        function messageCallback(e) {
            Ti.API.info("messageCallback: " + JSON.stringify(e));
            var message;
            if (e.inBackground == false) {
                if (e['data'] != undefined) {
                    if (e['data']['aps'] != undefined) {
                        if (e['data']['aps']['alert'] != undefined) {
                            message = e['data']['aps']['alert'];
                        } else {
                            message = e['data']['alert'];
                        }
                    } else {
                        message = e['data']['alert'];
                    }
                } else {
                    message = L('alloy_no_data');
                }
                alert(message);
            } else {
                Ti.App.fireEvent('pushrev');
            }
        }


        Ti.App.iOS.addEventListener('usernotificationsettings', function registerForPush() {

            // Remove event listener once registered for push notifications
            Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);

            Ti.Network.registerForPushNotifications({
                success : successCallback,
                error : errorCallback,
                callback : messageCallback
            });
        });

        // Register notification types to use
        Ti.App.iOS.registerUserNotificationSettings({
            types : [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
        });
    };
    notification();
} else if (OS_ANDROID) {
    var gcm = require('net.iamyellow.gcmjs');

    var pendingData = gcm.data;

    if (pendingData && pendingData !== null) {
        // if we're here is because user has clicked on the notification
        // and we set extras for the intent
        // and the app WAS NOT running
        // (don't worry, we'll see more of this later)
        Ti.API.info('******* data (started) ' + JSON.stringify(pendingData));
    }

    gcm.registerForPushNotifications({
        success : function(ev) {
            // on successful registration
            Ti.App.Properties.setString('deviceToken', ev.deviceToken);
            Ti.API.info('******* success, ' + ev.deviceToken);
        },
        error : function(ev) {
            // when an error occurs
            Ti.API.info('******* error, ' + ev.error);
        },
        callback : function(ev) {
            // when a gcm notification is received WHEN the app IS IN FOREGROUND
            Ti.API.info('callback' + JSON.stringify(ev));

            var title = ev.title || 'Bromley Well';
            var message = ev.message;
            var appdata = ev.appdata || {};
            var notificationId = (function() {
                var str = '',
                    now = new Date();

                var hours = now.getHours(),
                    minutes = now.getMinutes(),
                    seconds = now.getSeconds();
                str += (hours > 11 ? hours - 12 : hours) + '';
                str += minutes + '';
                str += seconds + '';

                var start = new Date(now.getFullYear(), 0, 0),
                    diff = now - start,
                    oneDay = 1000 * 60 * 60 * 24,
                    day = Math.floor(diff / oneDay);
                str += day * (hours > 11 ? 2 : 1);

                var ml = (now.getMilliseconds() / 100) | 0;
                str += ml;
                return str | 0;
            })();
            var ntfId = Ti.App.Properties.getInt('ntfId', 0),
                launcherIntent = Ti.Android.createIntent({
                className : 'net.iamyellow.gcmjs.GcmjsActivity',
                action : 'action' + ntfId, // we need an action identifier to be able to track click on notifications
                packageName : Ti.App.id,
                flags : Ti.Android.FLAG_ACTIVITY_NEW_TASK | Ti.Android.FLAG_ACTIVITY_SINGLE_TOP
            });
            launcherIntent.addCategory(Ti.Android.CATEGORY_LAUNCHER);
            launcherIntent.putExtra("ntfId", ntfId);
            launcherIntent.putExtra("title", title);
            launcherIntent.putExtra("message", message);
            launcherIntent.putExtra("appdata", appdata);

            Ti.API.info('service gcm.js title' + title);
            Ti.API.info('service gcm.js message' + message);
            Ti.API.info('service gcm.js appdata' + appdata);
            Ti.API.info('service gcm.js ntfId :' + ntfId);
            ntfId += 1;

            var pintent = Ti.Android.createPendingIntent({
                intent : launcherIntent
            });
            var notification = Ti.Android.createNotification({
                contentIntent : pintent,
                contentTitle : title,
                contentText : message,
                defaults : Ti.Android.DEFAULT_ALL,
                tickerText : title, // shows on status bar when received. (upto Kitkat)
                icon : Ti.App.Android.R.drawable.appicon,
                flags : Ti.Android.FLAG_AUTO_CANCEL | Ti.Android.FLAG_SHOW_LIGHTS
            });

            Ti.Android.NotificationManager.notify(notificationId, notification);
        },
        unregister : function(ev) {
            // on unregister
            Ti.API.info('******* unregister, ' + ev.deviceToken);
        },
        data : function(data) {
            // if we're here is because user has clicked on the notification
            // and we set extras in the intent
            // and the app WAS RUNNING (=> RESUMED)
            // (again don't worry, we'll see more of this later)
            Ti.API.info('******* data (resumed) ' + JSON.stringify(data));
            Ti.App.fireEvent('pushrev');
        }
    });
}
