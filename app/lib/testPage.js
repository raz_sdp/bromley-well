var all_data = {
    Categories : [{
        "id" : "1",
        "name" : "Richter Scale",
        "is_deleted" : false,
        "questions" : [{
            "id" : "1",
            "category_id" : "1",
            "question" : "How do you feel you are doing at school? Think about : the marks you are getting, homework, attendance, friendships, bullying, teacher support, sport and hobbies etc",
            "is_deleted" : false            
        }, {
            "id" : "2",
            "category_id" : "1",
            "question" : "How do you feel about your emotional life? Think about: are you coping well, do your have mood swings, do you worry, do you get stressed, do you get angry easily",
            "is_deleted" : false
        }, {
            "id" : "3",
            "category_id" : "1",
            "question" : "How do you feel about your social life? Think about things you are involved in : sports, hobbies, friendships, ",
            "is_deleted" : false
        }, {
            "id" : "4",
            "category_id" : "1",
            "question" : "How do you feel about home and your caring role: Think about: are you doing too much, do you have to do things you don\u2019t like doing, do you get enough support, can you have a break when you need it, does caring interfere too much with the rest of your life",
            "is_deleted" : false
        }, {
            "id" : "5",
            "category_id" : "1",
            "question" : "How do you feel your weight",
            "is_deleted" : false
        }, {
            "id" : "6",
            "category_id" : "1",
            "question" : "How do you feel about physical fitness",
            "is_deleted" : false
        }, {
            "id" : "7",
            "category_id" : "1",
            "question" : "How do you feel about your dental hygiene?",
            "is_deleted" : false
        }, {
            "id" : "8",
            "category_id" : "1",
            "question" : "How do you feel your hearing ?",
            "is_deleted" : false
        }, {
            "id" : "9",
            "category_id" : "1",
            "question" : "How do you feel about your knowledge of smoking drugs and alcohol issues",
            "is_deleted" : false
        }, {
            "id" : "10",
            "category_id" : "1",
            "question" : "How do you feel about your knowledge of contraception and sexual health issues",
            "is_deleted" : false
        }, {
            "id" : "11",
            "category_id" : "1",
            "question" : "How do you feel about your knowledge of healthy eating ",
            "is_deleted" : false
        }, {
            "id" : "12",
            "category_id" : "1",
            "question" : "How do you feel your knowledge of safe practices in the home \u2013 lifting, kitchen safety etc",
            "is_deleted" : false
        }, {
            "id" : "13",
            "category_id" : "1",
            "question" : "How do you feel about your cookery skills",
            "is_deleted" : false
        }, {
            "id" : "14",
            "category_id" : "1",
            "question" : "How do you feel about your cleaning skills",
            "is_deleted" : false
        }, {
            "id" : "15",
            "category_id" : "1",
            "question" : "How do you feel about your organisation skills ",
            "is_deleted" : false
        }, {
            "id" : "16",
            "category_id" : "1",
            "question" : "How do you feel your general caring skills",
            "is_deleted" : false
        }]
    }, {
        "id" : "2",
        "name" : "How Caring Affects Me",
        "is_deleted" : false,
        "questions" : [{
            "id" : "17",
            "category_id" : "2",
            "question" : "Because of caring I feel I am doing something good",
            "is_deleted" : false
        }, {
            "id" : "18",
            "category_id" : "2",
            "question" : "Because of caring I feel I am helping",
            "is_deleted" : false
        }, {
            "id" : "19",
            "category_id" : "2",
            "question" : "Because of caring I feel closer to my family",
            "is_deleted" : false
        }, {
            "id" : "20",
            "category_id" : "2",
            "question" : "Because of caring I feel good about myself",
            "is_deleted" : false
        }, {
            "id" : "21",
            "category_id" : "2",
            "question" : "Because of caring I have to do things that make me upset",
            "is_deleted" : false
        }, {
            "id" : "22",
            "category_id" : "2",
            "question" : "Because of caring I have trouble staying awake",
            "is_deleted" : false
        }, {
            "id" : "23",
            "category_id" : "2",
            "question" : "Because of caring I feel I am better able to cope with problems",
            "is_deleted" : false
        }, {
            "id" : "24",
            "category_id" : "2",
            "question" : "I feel good about helping",
            "is_deleted" : false
        }, {
            "id" : "25",
            "category_id" : "2",
            "question" : "Because of caring I feel helpful",
            "is_deleted" : false
        }]
    }, {
        "id" : "3",
        "name" : "Young Carer Feedback Form",
        "is_deleted" : false,
        "questions" : [{
            "id" : "26",
            "category_id" : "3",
            "question" : "My AYC worker listens to me",
            "is_deleted" : false
        }, {
            "id" : "27",
            "category_id" : "3",
            "question" : "My AYC worker respects to my views",
            "is_deleted" : false
        }, {
            "id" : "28",
            "category_id" : "3",
            "question" : "I feel supported by AYC",
            "is_deleted" : false
        }, {
            "id" : "29",
            "category_id" : "3",
            "question" : "I understand the information given to me",
            "is_deleted" : false
        }, {
            "id" : "30",
            "category_id" : "3",
            "question" : "I enjoy the activities",
            "is_deleted" : false
        }, {
            "id" : "31",
            "category_id" : "3",
            "question" : "The AYC team are easy to contact and approachable",
            "is_deleted" : false
        }, {
            "id" : "32",
            "category_id" : "3",
            "question" : "AYC helps me by...",
            "is_deleted" : false
        }, {
            "id" : "33",
            "category_id" : "3",
            "question" : "The best thing AYC offers me",
            "is_deleted" : false
        }, {
            "id" : "34",
            "category_id" : "3",
            "question" : "How could AYC improve?",
            "is_deleted" : false
        }, {
            "id" : "35",
            "category_id" : "3",
            "question" : "Any other comments",
            "is_deleted" : false
        }]
    }],
    "Document" : [{
        "id" : "1",
        "title" : "old",
        "file" : "http:\/\/74.3.255.228\/carers\/files\/documents\/1401879255914.pdf",
        "is_deleted" : false
    }],

    "Information" : [{
        "about_company" : "about company"
    }],

    "Notification" : [],
    "download_time" : 1402202082
};

