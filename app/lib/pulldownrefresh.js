exports.tablePullHeader = function(table, loadFunction) {
    var tableHeader = Ti.UI.createView({
        backgroundColor : 'transparent',
        width : 320,
        height : 70
    });

    var imageArrow = Ti.UI.createImageView({
        image : '/images/whiteArrow.png',
        left : 20,
        bottom : 5,
        width : 23,
        height : 60
    });
    tableHeader.add(imageArrow);

    var labelStatus = Ti.UI.createLabel({
        color : '#576c89',
        font : {
            fontSize : 13,
            fontWeight : 'bold'
        },
        text : 'Pull down to refresh...',
        textAlign : 'center',
        left : 55,
        bottom : 27,
        width : 200
    });
    tableHeader.add(labelStatus);


    var actInd = Ti.UI.createActivityIndicator({
        left : 20,
        bottom : 13,
        width : 30,
        height : 30
    });

    function resetPullHeader(table) {
        reloading = false;
        actInd.hide();
        imageArrow.transform = Ti.UI.create2DMatrix();
        imageArrow.show();
        labelStatus.text = 'Pull down to refresh...';
        table.setContentInsets({
            top : 0
        }, {
            animated : true
        });
    };

    tableHeader.add(actInd);
    var pulling = false;
    var reloading = false;
    var offset = 0;
    table.addEventListener('scroll', function(e) {
        offset = e.contentOffset.y;
        if (pulling && !reloading && offset > -80 && offset < 0) {
            pulling = false;
            var unrotate = Ti.UI.create2DMatrix();
            imageArrow.animate({
                transform : unrotate,
                duration : 180
            });
            labelStatus.text = 'Pull down to refresh...';
        } else if (!pulling && !reloading && offset < -80) {
            pulling = true;
            var rotate = Ti.UI.create2DMatrix().rotate(180);
            imageArrow.animate({
                transform : rotate,
                duration : 180
            });
            labelStatus.text = 'Release to refresh...';
        }
    });
    table.addEventListener('dragEnd', function(e) {
        if (pulling && !reloading && offset < -80) {
            pulling = false;
            reloading = true;
            labelStatus.text = 'Updating...';
            imageArrow.hide();
            actInd.show();
            e.source.setContentInsets({
                top : 80
            }, {
                animated : true
            });
            setTimeout(function() {
                loadFunction(resetPullHeader(e.source));
            }, 2000);
        }
    });
    table.headerPullView = tableHeader;
};