function extend_(destination, source) {
    _.each(source, function(d) {
        var index = destination.indexOf(_.findWhere(destination, {
            id : d.id
        }));
        if (index === -1)
            destination.unshift(d);
        else
            destination[index] = d;
    });
    return destination;
};
function downLoadResources(dataArray, callback) {
    var i = 0;
    function recursiveFunction(i) {
        if (i >= dataArray.length) {
        } else {
            var isOnline = Alloy.Globals.xhr.isOnline();
            var fileUrl = dataArray[i];
            var stringParts = fileUrl.split('/'),
                f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, stringParts.pop());
            if (f.exists()) {
                f.remoteBackup = false;
                i++;
                isOnline = fileUrl = stringParts = f = null;
                recursiveFunction(i);
            } else {
                if (isOnline) {
                    Alloy.Globals.xhr.getFile(fileUrl, function(fileResult) {
                        f.write(fileResult.data);
                        if (f.exists())
                            f.remoteBackup = false;
                        i++;
                        recursiveFunction(i);
                        isOnline = fileUrl = stringParts = f = null;
                    }, function() {
                        i++;
                        recursiveFunction(i);
                        isOnline = fileUrl = stringParts = f = null;
                    }, {}, function(progress) {
                        Ti.API.info('progress ' + progress);
                    });
                } else {
                    isOnline = fileUrl = stringParts = f = null;
                }
            }
        }
    };
    recursiveFunction(i);
};
exports.downloadData = function(callback) {
    var files = [];
    var url = Alloy.Globals.allDataURL + Ti.App.Properties.getObject('user').id + '/' + Ti.App.Properties.getString("downloadTime", '');
    Ti.API.info('url downloadData' + url);
    if (Alloy.Globals.xhr.isOnline()) {
        Alloy.Globals.xhr.get(url, function(CFAllData) {
            Ti.API.info("CFAllData == " + JSON.stringify(CFAllData));
            if (!!CFAllData.data.success) {
                if (Ti.App.Properties.getObject("CFData", null)) {
                    var CFData = Ti.App.Properties.getObject("CFData", {});
                    //Ti.API.info('old data === ' + JSON.stringify(IFCData));
                    _.each(CFData, function(element, model) {
                        if (model == 'download_time') {
                            CFData[model] = CFAllData.data.all_data[model];
                        } else if (model != 'Categories') {
                            CFData[model] = _.where(extend_(CFData[model], CFAllData.data.all_data[model]), {
                                is_deleted : false
                            });
                            if (model == 'Notification') {
                                CFData[model] = _.where(extend_(CFData[model], CFAllData.data.all_data[model]), {
                                    user_deleted : false
                                });
                            }
                            Ti.API.info('model  === ' + model + '   ' + JSON.stringify(CFAllData));
                            if (model == 'Whats_Happening') {
                                CFData[model] = CFAllData.data.all_data[model];
                            }
                            /*
                             if (model == 'Document') {
                             _.each(IFCData[model], function(doc) {
                             files.push(doc.filename);
                             });
                             } else if (model == 'Information') {
                             _.each(IFCData[model], function(doc) {
                             files.push(doc.multimedia);
                             });
                             }*/

                        } else {
                            if (CFAllData.data.all_data[model].length) {
                                Ti.API.info('dadad ' + JSON.stringify(CFData[model]));

                                _.each(CFData[model], function(category, index) {

                                    CFData[model][index].questions = _.where(extend_(CFData[model][index].questions, CFAllData.data.all_data[model][index].questions), {
                                        is_deleted : false
                                    });
                                    CFData[model][index] = _.extend(CFData[model][index], CFAllData.data.all_data[model][index]);

                                });
                            }
                            //files.push(IFCData[model].demo);
                        }

                    });
                    //Ti.API.info('files ' + JSON.stringify(files));
                    Ti.App.Properties.setObject("CFData", CFData);
                    Ti.API.info('new data === ' + JSON.stringify(CFData));
                    Ti.App.Properties.setString("downloadTime", CFData.download_time);
                    callback({
                        success : true
                    });
                } else {
                    var CFData = CFAllData.data.all_data;
                    /*_.each(IFCData, function(element, model) {
                    if (model == 'Document') {
                    _.each(IFCData[model], function(doc) {
                    files.push(doc.filename);
                    });
                    } else if (model == 'Information') {
                    _.each(IFCData[model], function(doc) {
                    files.push(doc.multimedia);
                    });
                    } else if (model == 'Company') {
                    //files.push(IFCData[model].demo);
                    }
                    });*/
                    //Ti.API.info('files ' + JSON.stringify(files));
                    //Ti.App.Properties.setBool('firstRun', false);
                    Ti.App.Properties.setObject("CFData", CFData);
                    Ti.App.Properties.setString("downloadTime", CFData.download_time);
                    callback({
                        success : true
                    });
                }
                //downLoadResources(files);
            } else {
                callback({
                    success : false,
                    msg : "Data is not available. Please try again later"
                });
            }
        }, function(errResult) {
            Ti.API.info(errResult);
            callback({
                success : false,
                msg : "Network request problem. Please try again later"
            });
        }, {});
    } else {
        callback({
            success : false,
            msg : "No Network Connection!"
        });
    }
};
exports.getLocalFile = function(url, callback) {
    Ti.API.info(url);
    var isOnline = Alloy.Globals.xhr.isOnline();
    var stringParts = url.split('/'),
        f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, stringParts.pop());
    if (f.exists()) {
        callback({
            success : true,
            url : f.nativePath
        });
    } else {
        if (isOnline) {
            Alloy.Globals.xhr.getFile(url, function(fileResult) {

                f.write(fileResult.data);
                if (f.exists())
                    f.remoteBackup = false;
                callback({
                    success : true,
                    url : f.nativrPath
                });

            }, function() {
                callback({
                    success : false,
                    msg : "Network request problem. Please try again later"
                });
            }, {}, function(progress) {
                Ti.API.info('progress ' + progress);
            });
        } else {
            callback({
                success : false,
                msg : "No Network Connection!"
            });
        }
    }
};
