var args = arguments[0] || {};
var moment = require('/alloy/moment');
var day = moment(args.notification.created);

$.MsgLabel.text = args.notification.message;
$.MsgMainView.xData = args.notification;
$.dateLabel.text = day.format("DD MMM HH:mm");

function msgCrossBtnClickFunction(e) {
    args.callback(e.source.notification_id);
}