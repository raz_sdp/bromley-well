var args = arguments[0] || {};
isBack = args.isBack || false;
$.titleLabel.text = args.title;
if (args.title == 'Signup') {
    $.leftButton.visible = false;
}
if (Alloy.Globals.isiPhoneX()) {
    $.container.top = 20;
}
$.leftButton.backgroundImage = isBack ? "/images/backArrowbtn.png" : "/images/about/homeIcon.png";
function leftButtonClickFunction(e) {
    if (args.callback) {
        args.callback();
    } else
        Alloy.Globals.b();
}

function rightButtonClickFunction(e) {
    Alloy.Globals.b();
}