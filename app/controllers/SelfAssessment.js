var args = arguments[0] || {};
$.feedbackWin.add(Alloy.createController('navbar', {
    title : ''
}).getView());

var buttons = [{
    title : 'Young Carers Assessment',
    top : 0,
    istitleDisplay : true,
    winName : 'RichterScale',
    image : '/images/feedbackmenu/feedback-menu.png',
    icon : '/images/feedback_menu/iconEmoji.png',
}, {
    title : 'How Caring Affects Me',
    top : 0,
    istitleDisplay : true,
    winName : 'feedbackHowCaring',
    image : '/images/feedbackmenu/feedback-menu.png',
    icon : '/images/feedback_menu/iconEmoji.png',
}];

_.each(buttons, function(buttonData) {
    $.buttonView.add(Alloy.createController('hexagonalButton', buttonData).getView());
});
$.feedbackWin.addEventListener('close', function() {
    $.destroy();
});
$.logotwiter.addEventListener('click', function() {
    Ti.Platform.openURL("https://twitter.com/BromleyWell_YC");
}); 