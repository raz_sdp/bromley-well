var args = arguments[0] || {};
$.notificationsWin.add(Alloy.createController('navbar', {
    title : 'Message Us'
}).getView());
var user = Ti.App.Properties.getObject('user', null);
var tableData = [],
    focusDeleteButton = false,
    focusedRow = null,
    animating = false,
    notificationId = 0;
var matrix = Ti.UI.create2DMatrix(),
    trx = matrix.scale(0, 0);
$.addResponseView.transform = trx;
$.addResponseView.width = 0;
$.addResponseView.height = 0;
function sendCallback(notification_id) {
    notificationId = notification_id;
    $.alertDialog.show();
}

function alertClickFunction(e) {
    if (e.index == 0) {
        if ($.addResponseTextArea.value) {
            var user = Ti.App.Properties.getObject('user', {});
            var data = {
                "data[Staffalert][user_id]" : user.id,
                "data[Staffalert][message]" : $.addResponseTextArea.value,
            };
            if (Alloy.Globals.xhr.isOnline()) {
                $.ai.show();

                Alloy.Globals.xhr.post(Alloy.Globals.submit_alert, data, function(successResult) {
                    Ti.API.info('notifications:::' + JSON.stringify(successResult));
                    var dialog = Ti.UI.createAlertDialog({
                        message : successResult.data.msg,
                        ok : 'Ok',
                        title : 'Info'
                    });
                    dialog.show();
                    $.addResponseTextArea.value = '';
                    $.ai.hide();
                    notificationFocusFunction();
                }, function(errorResult) {
                    Ti.API.info(JSON.stringify(errorResult));
                    $.ai.hide();
                    alert(errorResult);
                });

            } else {
                alert("No Network Connection!");
            }
            $.addResponseView.width = 0;
            $.addResponseView.height = 0;
            trx = matrix.scale(0, 0);
            $.addResponseView.animate({
                transform : trx,
                duration : 200
            });
        }
    }
}

function messageCloseFunction2() {
    trx = matrix.scale(0, 0);
    $.msgDetailView.width = 0;
    $.msgDetailView.height = 0;
    $.msgDetailView.animate({
        transform : trx,
        duration : 200
    });
}

function messageCloseFunction() {
    trx = matrix.scale(0, 0);
    $.addResponseView.width = 0;
    $.addResponseView.height = 0;
    $.addResponseView.animate({
        transform : trx,
        duration : 200
    });
}

function deleteClickFunction() {
    $.alertDialogdelete.show();
}

function alertDeleteClickFunction(e) {
    if (e.index == 0) {
        var user = Ti.App.Properties.getObject('user', {});
        var data = {
            "data[Staffalert][user_id]" : user.id,
            "data[Staffalert][carerid]" : user.carerid,
            "data[Staffalert][notification_id]" : notificationId,
        };
        if (Alloy.Globals.xhr.isOnline()) {
            $.ai.show();

            Alloy.Globals.xhr.post(Alloy.Globals.alertDeleteURL + notificationId, data, function(successResult) {
                Ti.API.info('notifications:::' + JSON.stringify(successResult));
                if (!!successResult.data.success) {
                    Ti.API.info('notifications tableData:::' + tableData.length);
                    notificationFocusFunction();
                } else {
                    alert(successResult.data.msg);
                }
                $.ai.hide();
            }, function(errorResult) {
                Ti.API.info(JSON.stringify(errorResult));
                $.ai.hide();
                alert(errorResult);
            });

        } else {
            alert("No Network Connection!");
        }
        $.msgDetailView.width = 0;
        $.msgDetailView.height = 0;
        trx = matrix.scale(0, 0);
        $.msgDetailView.animate({
            transform : trx,
            duration : 200
        });
    }
}

function saveClickFunction() {
    $.alertDialog.show();
}

function notificationFocusFunction(e) {

    if (!!Alloy.Globals.xhr.isOnline()) {
        $.ai.show();
        var user = Ti.App.Properties.getObject('user', {});
        var url = Alloy.Globals.staffalerts + user.id;
        Alloy.Globals.xhr.get(url, function(responceData) {
            $.ai.hide();
            var result = (responceData).data.result;
            Ti.API.info('active ' + JSON.stringify(responceData));
            tableData = [];
            _.each(result, function(_notification, _index) {
                _notification.index = _index;
                Ti.API.info('active ' + JSON.stringify(_notification));
                tableData.push(Alloy.createController("alertsCommonView", {
                    notification : _notification,
                    callback : sendCallback
                }).getView());
            });
            $.notificationsTable.setData(tableData);

        }, function(errResult) {
            $.ai.hide();
        }, {});

    } else {
        Alloy.Globals.isActive_data = {
            success : false,
            msg : "No internet connectivity is available, please try again later."
        };
    }
}

$.notificationsWin.addEventListener('close', function() {
    $.destroy();
});

function submitButtonClickFunction(e) {
    if (focusDeleteButton && !animating) {
        focusDeleteButton = false;
        focusedRow.getChildren()[2].animate({
            right : -86,
            duration : 100
        });
    } else if (!focusDeleteButton && !animating) {

        $.addResponseView.width = Ti.UI.FILL;
        $.addResponseView.height = 270;
        trx = matrix.scale(1, 1);
        $.addResponseView.animate({
            transform : trx,
            duration : 200
        });

    }
};
function tableClickFunction(e) {
    if (focusDeleteButton && !animating) {
        focusDeleteButton = false;
        focusedRow.getChildren()[2].animate({
            right : -86,
            duration : 100
        });
    } else if (!focusDeleteButton && !animating) {
        Ti.API.info('e.row.xData ' + JSON.stringify(e.row.xData));
        $.msgLabel.text = e.row.xData.message;
        notificationId = e.row.xData.id;
        $.msgDetailView.width = Ti.UI.FILL;
        $.msgDetailView.height = 270;
        trx = matrix.scale(1, 1);
        $.msgDetailView.animate({
            transform : trx,
            duration : 200
        });

    }
};
function textAreaReturnFunction(e) {

}