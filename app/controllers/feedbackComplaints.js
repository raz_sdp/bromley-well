var args = arguments[0] || {};

$.feedbackComplaintsWin.add(Alloy.createController('navbar', {
    title : 'Complaints',
    isBack : true
}).getView());

var matrix = Ti.UI.create2DMatrix(),
    trx = matrix.scale(0, 0);
$.complaintsDetailView.transform = trx;
function callButtonClickFunction() {
    Ti.API.info('callButtonClickFunction');
    Ti.Platform.openURL('tel:' + Ti.App.Properties.getObject("CFData", {}).Information[0].manager_phone);
};
function writeButtonClickFunction() {
    $.complaintsDetailView.height = 228;
    $.complaintsDetailView.width = 320;
    Ti.API.info('writeButtonClickFunction');
    trx = matrix.scale(1, 1);

    $.complaintsDetailView.animate({
        transform : trx,
        duration : 100
    });
    $.complaintsDetailTextArea.focus();
};
function cancelClickFunction() {
    trx = matrix.scale(0, 0);

    $.complaintsDetailView.animate({
        transform : trx,
        duration : 100
    });
    $.complaintsDetailTextArea.blur();
    $.complaintsDetailView.height = 0;
    $.complaintsDetailView.width = 0;
};

// Alloy.Globals.checkUserActivation();
function submitData() {
    // if (!Alloy.Globals.isActive_data.success) {
    // alert(Alloy.Globals.isActive_data.msg);
    // return false;
    // }
    var user = Ti.App.Properties.getObject('user', {});
    var data = {
        "data[Complaint][user_id]" : user.id,
        "data[Complaint][carerid]" : user.carerid,
        "data[Complaint][complaint]" : $.complaintsDetailTextArea.value
    };
    $.ai.show();
    if (Alloy.Globals.xhr.isOnline()) {
        Alloy.Globals.xhr.post(Alloy.Globals.complaintURL, data, function(successResult) {
            $.submitDialog.message = successResult.data.msg;
            $.submitDialog.show();
            $.ai.hide();
            // alert(successResult.data.msg);
        }, function(errorResult) {
            Ti.API.info(JSON.stringify(errorResult));
            $.ai.hide();
            alert(errorResult);
        });
    } else {
        alert('No network!');
    }
}

function submitClickFunction() {
    if ($.complaintsDetailTextArea.value == "") {
        alert("Please write something in the complaint box");
    } else {
        submitData();
        trx = matrix.scale(0, 0);
        $.complaintsDetailView.animate({
            transform : trx,
            duration : 100
        });
        $.complaintsDetailTextArea.blur();
    }
};
function submitDialogClick() {
    Alloy.Globals.winOpener("Home", true, {});
}

function textAreaReturnFunction(e) {
    if (!!e.source.value) {
        submitData();
    }
    trx = matrix.scale(0, 0);
    $.complaintsDetailView.animate({
        transform : trx,
        duration : 100
    });
}
