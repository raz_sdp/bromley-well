var args = arguments[0] || {};
var tableData = [];
$.feedbackHowCaringWin.add(Alloy.createController('navbar', {
    title : 'How Caring Affects Me',
    callback : showAlert,
    isBack : true
}).getView());

_.each(Ti.App.Properties.getObject("CFData", {}).Categories[1].questions, function(question) {
    tableData.push(Alloy.createController('feedbackRadioCommonView', question).getView());
});
if (Alloy.Globals.isiPhoneX()) {
    $.buttonView.height = 120;
}
$.howcaringTable.setData(tableData);

var postData = {};
function submitButtonClickFunction() {

    var rows = $.howcaringTable.getData()[0].getRows();
    _.each(rows, function(row, index) {
        postData['data[' + index + '][qustion_id]'] = row.xData.id;
        postData['data[' + index + '][answer_score]'] = row.value;
    });
    postData['data[category_id]'] = rows[0].xData.category_id;

    if (Alloy.Globals.xhr.isOnline()) {
        $.ai.show();
        Alloy.Globals.xhr.post(Alloy.Globals.feedbackSubmitURL + Ti.App.Properties.getObject("user", {}).id, postData, function(successResult) {
            Ti.API.info('Submit data of How Caring' + JSON.stringify(successResult));
            $.submitDialog.message = successResult.data.msg;
            $.submitDialog.show();
            $.ai.hide();
        }, function(errorResult) {
            Ti.API.info(JSON.stringify(errorResult));
            $.ai.hide();
            alert(errorResult);
        });
    } else {
        alert("No Network Connection!");
    }
};
$.feedbackHowCaringWin.addEventListener('close', function() {
    $.destroy();
});
function submitDialogClick() {
    Alloy.Globals.winOpener("Home", false, {});
}

function showAlert() {
    var dialog = Ti.UI.createAlertDialog({
        cancel : 1,
        buttonNames : ['Yes', 'Cancel'],
        message : 'Your feedback has not been submitted, are you sure you want exit?',
        title : 'Warning'
    });
    dialog.addEventListener('click', function(e) {
        if (e.index === e.source.cancel) {
            Ti.API.info('The cancel button was clicked');
        } else {
            Alloy.Globals.b();
        }
    });
    dialog.show();

}