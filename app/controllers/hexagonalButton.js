var args = arguments[0] || {};
Ti.API.info('args' + JSON.stringify(args));
$.buttonContainer.top = args.top;
$.buttonContainer.left = args.left;
$.buttonContainer.right = args.right;
$.buttonContainer.data = args;
$.commonButton.text = args.title;
$.buttonContainer.backgroundImage = args.image;
$.icon.visible = true;
$.icon.image = args.icon;
if (args.istitleDisplay || false) {
    $.commonButton.width = 200;
    $.buttonContainer.width = 300;
    $.commonButton.bottom = 25;
    $.commonButton.left = 30;
    $.commonButton.height = 30;
}
function commonButtonClickFunction(e) {

    if (e.source.data.winName == "SelfAssessment" || e.source.data.winName == "Feedback" || e.source.data.winName == "Notifications") {
        if (!!Alloy.Globals.isActive_data.success) {
            Alloy.Globals.winOpener(e.source.data.winName, false, e.source.data);
        } else {
            alert(Alloy.Globals.isActive_data.msg);
        }
    } else if (e.source.data.winName == "Downloads") {
        if (!!Alloy.Globals.xhr.isOnline()) {
            if (!!Alloy.Globals.isActive_data.success) {
                Alloy.Globals.winOpener(e.source.data.winName, false, e.source.data);
            } else {
                alert(Alloy.Globals.isActive_data.msg);
            }
        } else {
            Alloy.Globals.winOpener(e.source.data.winName, false, e.source.data);
        }
    } else if (e.source.data.winName == "NewsEvents" && !Alloy.Globals.xhr.isOnline()) {
        alert("No internet connectivity is available, please try again later.");
    } else {
        Alloy.Globals.winOpener(e.source.data.winName, false, e.source.data);
    }
}
