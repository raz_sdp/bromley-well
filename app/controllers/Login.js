var args = arguments[0] || {};

function LoginButtonClickFunction() {
    if ($.userNameTextField.value == '' || $.passTextField.value == "") {
        var dialog = Ti.UI.createAlertDialog({
            message : 'Incorrect username or password, please try again!',
            ok : 'OK',
            title : 'Warning'
        });
        dialog.show();
    } else {
        var data = {
            "data[User][name]" : $.userNameTextField.value,
            "data[User][password]" : $.passTextField.value,
            "data[DeviceToken][device_token]" : Ti.App.Properties.getString('deviceToken', 'c3ae02e6b758ae28101697f53dc794f46957441151310644d9d2bb719e0fbe4a'),
            "data[DeviceToken][device_type]" : OS_IOS ?"ios": 'android',
            "data[DeviceToken][stage]" : Ti.App.deployType //"development"
        };
        if (Alloy.Globals.xhr.isOnline()) {
            $.ai.show();
            Alloy.Globals.xhr.post(Alloy.Globals.root + 'users/login', data, function(successResult) {
                Ti.API.info(JSON.stringify(successResult));
                if (!!successResult.data.success) {
                    Ti.App.Properties.setObject('user', successResult.data.login);
                    $.ai.show();
                    Alloy.Globals.dataSynchronize(function(success) {
                        if (!!success.success) {
                            Alloy.Globals.winOpener("Home", false, {});
                        } else {
                            alert(success.msg);
                        }
                        $.ai.hide();
                    });
                } else {
                    var dialog = Ti.UI.createAlertDialog({
                        message : 'Incorrect username or password, please try again!',
                        ok : 'OK',
                        title : 'Warning'
                    });
                    dialog.show();
                }
                $.ai.hide();
            }, function(errorResult) {
                Ti.API.info(JSON.stringify(errorResult));
                $.ai.hide();
                var dialog = Ti.UI.createAlertDialog({
                    message : errorResult,
                    ok : 'OK',
                    title : 'Warning'
                });
                dialog.show();
            });

        } else {
            var dialog = Ti.UI.createAlertDialog({
                message : 'No Network Connection!',
                ok : 'OK',
                title : 'Warning'
            });
            dialog.show();
        }
    }
}

function forgetButtonClickFunction() {
    $.forgetpassAlert.show();
}

function forgetClickFunction(e) {
    var email;
    if (OS_IOS) {
        email = e.text || "";
    } else {
        email = $.emailTextField.value || "";
    }
    if (email) {
        var data = {
            "data[User][email]" : email
        };
        if (Alloy.Globals.xhr.isOnline()) {
            Alloy.Globals.xhr.post(Alloy.Globals.root + 'users/forget_password', data, function(successResult) {
                Ti.API.info(JSON.stringify(successResult));
                if (!!successResult.data.success) {
                    var dialog = Ti.UI.createAlertDialog({
                        message : successResult.data.msg,
                        ok : 'OK',
                        title : 'Success'
                    });
                    dialog.show();
                }
            }, function(errorResult) {
                Ti.API.info(JSON.stringify(errorResult));
                $.ai.hide();
                var dialog = Ti.UI.createAlertDialog({
                    message : errorResult,
                    ok : 'OK',
                    title : 'Warning'
                });
                dialog.show();
            });
        } else {
            var dialog = Ti.UI.createAlertDialog({
                message : 'No Network Connection!',
                ok : 'OK',
                title : 'Warning'
            });
            dialog.show();
        }
    } else {
        var dialog = Ti.UI.createAlertDialog({
            message : "All fields are required",
            ok : 'OK',
            title : 'Warning'
        });
        dialog.show();
    }
}

function signUpButtonClickFunction() {
    Alloy.Globals.winOpener("Signup", false, {});
}

function openSignUpWin() {
    $.userNameTextField.blur();
}

$.loginWin.addEventListener('close', function() {
    $.destroy();
});
if (Alloy.Globals.isiPhoneX()) {
    $.loginMainView.applyProperties({
        top : 40,
        layout : "vertical",
        bottom : 200
    });
    $.logo.applyProperties({
        top : 20,
        height : 200
    });
    $.loginBottomView.applyProperties({
        height : 200
    });

    $.userNameTextField.applyProperties({
        height : 70
    });

    $.passTextField.applyProperties({
        height : 70
    });

    $.userNameView.applyProperties({
        height : 80
    });

    $.passView.applyProperties({
        height : 80
    });
}
