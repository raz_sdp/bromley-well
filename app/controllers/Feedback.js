var args = arguments[0] || {};
$.feedbackWin.add(Alloy.createController('navbar', {
    title : ''
}).getView());
var buttons = [{
    title : 'Young Carers Feedback',
    top : 0,
    winName : 'feedbackYoungCarer',
    istitleDisplay : true,
    image : '/images/feedbackmenu/feedback-menu.png',
    icon: '/images/feedback_menu/iconEmoji.png', 
}/*, {
    title : 'Complaints',
    top : 11,
    winName : 'feedbackComplaints',
    istitleDisplay : true,
    image : '/images/feedbackmenu/feedback-menu.png',
    icon: '/images/feedback_menu/iconEmoji.png', 
}*/];

_.each(buttons, function(buttonData) {
    $.buttonView.add(Alloy.createController('hexagonalButton', buttonData).getView());
});
$.logotwiter.addEventListener('click', function() {
    Ti.Platform.openURL("https://twitter.com/BromleyWell_YC");
});