var args = arguments[0] || {};
$.feedbackYoungCarerWin.add(Alloy.createController('navbar', {
    title : 'Young Carers Feedback',
    callback : showAlert,
    isBack : true
}).getView());
if (Alloy.Globals.isiPhoneX()) {
    $.buttonView.height = 120;
    $.YoungCarerTable.bottom = 120;
}
var matrix = Ti.UI.create2DMatrix(),
    trx = matrix.scale(0, 0);
$.addResponseView.transform = trx;
var selectedHowButton = null;
var tableData = [];

function addResponseClick(button) {
    selectedHowButton = button;
    $.addResponseTextArea.value = selectedHowButton.xText;
    trx = matrix.scale(1, 1);
    $.addResponseView.height = 250;
    $.addResponseView.width = Ti.UI.FILL;
    $.addResponseView.animate({
        transform : trx,
        duration : 100
    });
    $.addResponseTextArea.focus();

}

_.each(Ti.App.Properties.getObject("CFData", {}).Categories[2].questions, function(question, _index) {
    if (question.ans_type == "numeric") {
        question.index = _index;
        tableData.push(Alloy.createController('feedbackSliderCommonView', question).getView());
    } else {
        tableData.push(Alloy.createController('feedbackAddResponseCommonView', {
            callback : addResponseClick,
            question : question
        }).getView());
    }

});

$.YoungCarerTable.setData(tableData);

function cancelClickFunction(e) {
    trx = matrix.scale(0, 0);
    $.addResponseView.height = 0;
    $.addResponseView.width = 0;
    $.addResponseView.animate({
        transform : trx,
        duration : 100
    });
    $.addResponseTextArea.blur();
}

function saveClickFunction() {
    if ($.addResponseTextArea.value == '') {
        alert('Text can\'t be saved as blank');
        selectedHowButton.text = "Type your answer here...";
    } else {
        selectedHowButton.xText = $.addResponseTextArea.value;
        selectedHowButton.text = $.addResponseTextArea.value;
        trx = matrix.scale(0, 0);
        $.addResponseView.height = 0;
        $.addResponseView.width = 0;
        $.addResponseView.animate({
            transform : trx,
            duration : 100
        });
        $.addResponseTextArea.blur();
    }

}

function textAreaReturnFunction(e) {
    if (!!e.source.value) {
        selectedHowButton.xText = $.addResponseTextArea.value;
    } else {
        selectedHowButton.xText = '';
    }
    trx = matrix.scale(0, 0);
    $.addResponseView.height = 0;
    $.addResponseView.width = 0;
    $.addResponseView.animate({
        transform : trx,
        duration : 100
    });
}

var postData = {};

// Alloy.Globals.checkUserActivation();

function submitButtonClickFunction() {

    var rows = $.YoungCarerTable.getData()[0].getRows(),
        blankText = false;
    _.each(rows, function(row, index) {
        postData['data[' + index + '][qustion_id]'] = row.xData.id;
        if (row.xData.ans_type == "numeric") {
            postData['data[' + index + '][answer_score]'] = row.getChildren()[2].getChildren()[1].value;
        } else {
            if (row.getChildren()[1].xText == '') {
                blankText = true;
            }
            postData['data[' + index + '][answer]'] = row.getChildren()[1].xText;
        }
    });
    postData['data[category_id]'] = rows[0].xData.category_id;

    if (!!blankText) {
        alert('Please answer all questions before submission');
        return false;
    }

    if (Alloy.Globals.xhr.isOnline()) {
        $.ai.show();
        Alloy.Globals.xhr.post(Alloy.Globals.feedbackSubmitURL + Ti.App.Properties.getObject("user", {}).id, postData, function(successResult) {
            Ti.API.info('Submit data of Young carer' + JSON.stringify(successResult));
            $.submitDialog.message = successResult.data.msg;
            $.submitDialog.show();
            $.ai.hide();
        }, function(errorResult) {
            Ti.API.info(JSON.stringify(errorResult));
            $.ai.hide();
            alert(errorResult);

        });
    } else {
        alert("No Network Connection!");
    }

    Ti.API.info('table Data  ' + JSON.stringify(postData));
};

function submitDialogClick() {
    Alloy.Globals.winOpener("Home", false, {});
}

function showAlert() {
    var dialog = Ti.UI.createAlertDialog({
        cancel : 1,
        buttonNames : ['Yes', 'Cancel'],
        message : 'Your feedback has not been submitted, are you sure you want exit?',
        title : 'Warning'
    });
    dialog.addEventListener('click', function(e) {
        if (e.index === e.source.cancel) {
            Ti.API.info('The cancel button was clicked');
        } else {
            Alloy.Globals.b();
        }
    });
    dialog.show();

}