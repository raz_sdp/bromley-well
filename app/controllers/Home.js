var args = arguments[0] || {};

var buttons = [{
    title : 'Self Assesment',
    top : 0,
    left : 5,
    winName : 'SelfAssessment',
    right : null,
    image : '/images/home/menu-box.png',
    icon : '/images/home/selfButton.png'
}, {
    title : 'Awards',
    top : 0,
    left : 5,
    winName : 'AwardsAchievements',
    right : null,
    image : '/images/home/menu-box.png',
    icon : '/images/home/awardsBtn.png'
}, {
    title : 'Downloads',
    top : 0,
    left : 5,
    winName : 'Downloads',
    right : null,
    icon : '/images/home/download.png',
    image : '/images/home/menu-box.png',
}, {
    title : 'What\'s Happening',
    top : 0,
    left : 5,
    winName : 'NewsEvents',
    right : null,
    image : '/images/home/menu-box.png',
    icon : '/images/home/news.png'
}, {
    title : 'Notifications',
    top : 0,
    left : 5,
    winName : 'Notifications',
    right : null,
    image : '/images/home/menu-box.png',
    icon : '/images/home/notifications.png'
}, {
    title : 'Feedback',
    top : 0,
    left : 5,
    winName : 'Feedback',
    right : null,
    image : '/images/home/menu-box.png',
    icon : '/images/home/feedback.png'
}, {
    title : 'Message Us',
    top : 0,
    left : 5,
    winName : 'StaffAlert',
    right : null,
    image : '/images/home/menu-box.png',
    icon : '/images/home/staff-alert.png'
}, {
    title : 'Thought Tree',
    top : 0,
    left : 5,
    winName : 'ThoughtTree',
    right : null,
    image : '/images/home/menu-box.png',
    icon : '/images/home/tree.png'
}, {
    title : 'About Us',
    top : 0,
    left : 5,
    winName : 'AboutUs',
    right : null,
    image : '/images/home/menu-box.png',
    icon : '/images/home/about.png'
}, {
    title : 'Settings',
    top : 0,
    left : 5,
    winName : 'Settings',
    right : null,
    image : '/images/home/menu-box.png',
    icon : '/images/home/settings.png'
}];

_.each(buttons, function(buttonData) {
    $.buttonContainerView.add(Alloy.createController('hexagonalButton', buttonData).getView());
});
var intervalTimer = null;
function homeWinFocusFunction() {
    Ti.API.info('home focus');
    getAwardNumber();
    Alloy.Globals.checkUserActivation();
    intervalTimer = setInterval(function() {
        getAwardNumber();
        Alloy.Globals.checkUserActivation();
    }, 30000);
}

function homeWinBlurFunction() {
    Ti.API.info('home blur');
    if (!!intervalTimer) {
        clearInterval(intervalTimer);
        intervalTimer = null;
    }
}

function getAwardNumber() {
    if (!!Alloy.Globals.xhr.isOnline()) {
        var user = Ti.App.Properties.getObject('user');
        if (!!user && user.id) {
            var url = Alloy.Globals.awardsURL + user.id;
            Alloy.Globals.xhr.get(url, function(responseData) {
                if (!!responseData.data.success) {
                    setAwardNumber(responseData.data.total_award);
                } else {

                }
            }, function(errorData) {
                Ti.API.info('errorData ' + JSON.stringify(errorData));
            }, {});
        }
    } else {

    }
};
function setAwardNumber(awardsNumber) {
    var awardCountLabel = $.buttonContainerView.getChildren()[1].children[2];
    Ti.API.info('getChildren()[1] ' + JSON.stringify(awardCountLabel));
    var awardData = Ti.App.Properties.getObject('awardsInfo');
    if (awardsNumber > 0 && awardData != null && awardsNumber != awardData.total_award) {
        awardCountLabel.text = awardsNumber - awardData.total_award;
        awardCountLabel.visible = true;
    } else if (awardsNumber > 0 && awardData == null) {
        awardCountLabel.text = awardsNumber;
        awardCountLabel.visible = true;
    } else {
        awardCountLabel.text = "";
        awardCountLabel.visible = false;
    }
    Ti.App.Properties.setString('awardsNumber', awardsNumber);
}

Ti.App.addEventListener('pushrev', commonButtonClickFunction);
function commonButtonClickFunction(e) {
    var data = {
        title : 'Notifications',
        top : 0,
        left : 5,
        winName : 'Notifications',
        right : null,
        image : '/images/home/menu-box.png',
        icon : '/images/home/notifications.png'
    };

    if (!!Alloy.Globals.isActive_data.success) {
        Alloy.Globals.winOpener('Notifications', false, data);
    } else {
        alert(Alloy.Globals.isActive_data.msg);
    }
}

$.logotwiter.addEventListener('click', function() {
    Ti.Platform.openURL("https://twitter.com/BromleyWell_YC");
});
