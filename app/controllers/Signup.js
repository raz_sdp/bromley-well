function signUpButtonClickFunction() {
    if ($.userNameTextField.value == '' || $.mobNoTextField.value == "" || $.userIdTextField.value == "" || $.passTextField.value == "") {
        var dialog = Ti.UI.createAlertDialog({
            message : "All fields are required",
            ok : 'OK',
            title : 'Warning'
        });
        dialog.show();
    } else if ($.passTextField.value != $.confirmPassNameTextField.value) {
        var dialog = Ti.UI.createAlertDialog({
            message : "Passwords do not match, please try again",
            ok : 'OK',
            title : 'Warning'
        });
        dialog.show();
    } else {
        var data = {
            "data[User][name]" : $.userNameTextField.value,
            "data[User][password]" : $.passTextField.value,
            "data[User][mobile]" : $.mobNoTextField.value,
            "data[User][carerid]" : $.userIdTextField.value,
            "data[DeviceToken][device_token]" : Ti.App.Properties.getString('deviceToken', 'c3ae02e6b758ae28101697f53dc794f46957441151310644d9d2bb719e0fbe4a'),
            "data[DeviceToken][device_type]" : "ios",
            "data[DeviceToken][stage]" : Ti.App.deployType // "development",
        };
        if (Alloy.Globals.xhr.isOnline()) {
            $.ai.show();
            Alloy.Globals.xhr.post(Alloy.Globals.root + 'users/register', data, function(successResult) {

                if (!!successResult.data.success) {
                    Ti.App.Properties.setObject('user', successResult.data.user);
                    $.ai.show();
                    Alloy.Globals.dataSynchronize(function(success) {
                        if (!!success.success) {
                            Alloy.Globals.winOpener("Home", false, {});
                        } else {
                            alert(success.msg);
                        }
                        $.ai.hide();
                    });
                } else {
                    alert(successResult.data.msg);
                }
                $.ai.hide();
            }, function(errorResult) {
                Ti.API.info(JSON.stringify(errorResult));
                $.ai.hide();
                var dialog = Ti.UI.createAlertDialog({
                    message : errorResult,
                    ok : 'OK',
                    title : 'Warning'
                });
                dialog.show();
            });

        } else {
            var dialog = Ti.UI.createAlertDialog({
                message : 'No Network Connection!',
                ok : 'OK',
                title : 'Warning'
            });
            dialog.show();
        }
    }

}

function backToLogin() {
    $.signUpWin.close();
}

function openSignUpWin() {
    $.userNameTextField.blur();
}

$.signUpWin.addEventListener('close', function() {
    $.destroy();
});
