var args = arguments[0] || {};
$.AwardsAchievementsWin.add(Alloy.createController('navbar', {
    title : 'Awards'
}).getView());

function LoadData() {
    var awardsNumber = Ti.App.Properties.getString('awardsNumber');
    var awardData = Ti.App.Properties.getObject('awardsInfo');
    Ti.API.info(' awardData' + JSON.stringify(awardData));

    if (awardData == null) {
        ApiCall();
    } else {
        $.awardInfoLabel.text = "You've gained " + awardData.total_award + " out of " + Object.keys(awardData.award_info).length + " awards!";
        if (awardsNumber == awardData.total_award) {
            AwardsAchieved(awardData.award_info);
        } else {
            ApiCall();
        }
    }
};
LoadData();

function ApiCall() {
    $.ai.show();
    if (!!Alloy.Globals.xhr.isOnline()) {
        var user = Ti.App.Properties.getObject('user');
        if (!!user && user.id) {
            var url = Alloy.Globals.awardsURL + user.id;
            Alloy.Globals.xhr.get(url, function(responseData) {
                if (!!responseData.data.success) {
                    AwardsAchieved(responseData.data.award_info);
                    Ti.App.Properties.setObject('awardsInfo', responseData.data);
                    $.awardInfoLabel.text = "You've gained " + responseData.data.total_award + " out of " + Object.keys(responseData.data.award_info).length + " awards!";
                }
                $.ai.hide();
            }, function(errorData) {
                Ti.API.info('errorData ' + JSON.stringify(errorData));
                $.ai.hide();
            }, {});
        }
    }
}

function AwardsAchieved(award_info) {
    /*  award_info = {
     "bronze_award" : true,
     "silver_award" : true,
     "gold_award" : true,
     "carers_award" : true,
     "silver_feedback_award" : true,
     "gold_feedback_award" : true
     };*/
    if (award_info.bronze_award == true) {
        $.bronzeImageView.image = "/images/awards/bronze.png";
        $.broneAchievedLabel.visible = true;
    } else {
        $.bronzeImageView.image = "/images/awards/hiddenAward.png";
        $.broneAchievedLabel.visible = false;
        $.bronzeAwardBox.opacity = '.60';
    }

    if (award_info.silver_award == true) {
        $.silverImageView.image = "/images/awards/silver.png";
        $.silverAchievedLabel.visible = true;
    } else {
        $.silverImageView.image = "/images/awards/hiddenAward.png";
        $.silverAchievedLabel.visible = false;
        $.silverAwardBox.opacity = '.60';
    }

    if (award_info.gold_award == true) {
        $.goldImageView.image = "/images/awards/gold.png";
        $.goldAchievedLabel.visible = true;
    } else {
        $.goldImageView.image = "/images/awards/hiddenAward.png";
        $.goldAchievedLabel.visible = false;
        $.goldAwardBox.opacity = '.60';
    }

    if (award_info.carers_award == true) {
        $.carerImageView.image = "/images/awards/carers.png";
        $.carerAchievedLabel.visible = true;
    } else {
        $.carerImageView.image = "/images/awards/hiddenAward.png";
        $.carerAchievedLabel.visible = false;
        $.carerAwardBox.opacity = '.60';
    }

    if (award_info.silver_feedback_award == true) {
        $.silverFeedbackImageView.image = "/images/awards/silver-feedback.png";
        $.silverFeedbackAchievedLabel.visible = true;
        $.silverFeedbackAchievedLabel.applyProperties({
            height : Ti.UI.SIZE,
            width : Ti.UI.SIZE
        });
    } else {
        $.silverFeedbackImageView.image = "/images/awards/hiddenAward.png";
        $.silverFeedbackAchievedLabel.visible = false;
        $.silverFeedbackAwardBox.opacity = '.60';
    }

    if (award_info.gold_feedback_award == true) {
        $.goldFeedbackImageView.image = "/images/awards/gold-feedback.png";
        $.goldFeedbackAchievedLabel.visible = true;
        $.goldFeedbackAchievedLabel.applyProperties({
            height : Ti.UI.SIZE,
            width : Ti.UI.SIZE
        });
    } else {
        $.goldFeedbackImageView.image = "/images/awards/hiddenAward.png";
        $.goldFeedbackAchievedLabel.visible = false;
        $.goldFeedbackAwardBox.opacity = '.60';
    }
};
$.AwardsAchievementsWin.addEventListener('close', function() {
    $.destroy();
});
