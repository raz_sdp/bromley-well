var args = arguments[0] || {};

var user = Ti.App.Properties.getObject('user');
Ti.API.info('user  ' + JSON.stringify(user));
$.createUserLabel.text = "Signed in as " + user.name;
$.createVerLabel.text = "Version 1.0.0" ;//+ Ti.App.version;
$.settingsWin.add(Alloy.createController('navbar', {
    title : 'Settings'
}).getView());

function logoutButtonClickFunction() {
    Ti.App.Properties.setObject('user', null);
    Alloy.Globals.winOpener("Login", false, {});
}

$.settingsWin.addEventListener('close', function() {
    $.destroy();
});
