var args = arguments[0] || {};

$.quesLabel.text = args.question.question;
$.quesMainView.xData = args.question;
$.howButton.question_id = args.question.id;
$.howButton.xText = '';
var lefts = [3, 19, 18, 18, 18, 18, 19, 17, 18, 15];
for (var i = 0; i < 10; i++) {
    $.numberView1.add(Ti.UI.createLabel({
        text : i + 1,
        color : '#308fdd',
        left : lefts[i],
        font : {
            fontSize : 10
        },
        textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER
    }));
    $.numberView2.add(Ti.UI.createLabel({
        text : i + 1,
        color : '#f62876',
        left : lefts[i],
        font : {
            fontSize : 10
        },
        textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER
    }));
}

function sliderTouchEndFunction(e) {
    // Ti.API.info('value '+JSON.stringify(e));
    e.source.value = Math.round(e.source.value);
    Ti.API.info(e.source.value);
}

function howButtonClickFunction(e) {
    args.callback(e.source);
}
