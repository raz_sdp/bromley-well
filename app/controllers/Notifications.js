var args = arguments[0] || {};
$.notificationsWin.add(Alloy.createController('navbar', {
    title : 'Notifications'
}).getView());

var tableData = [],
    focusDeleteButton = false,
    focusedRow = null,
    animating = false,
    notificationId = 0;
var matrix = Ti.UI.create2DMatrix(),
    trx = matrix.scale(0, 0);
$.msgDetailView.transform = trx;
$.msgDetailView.width = 0;
$.msgDetailView.height = 0;
function deleteCallback(notification_id) {
    notificationId = notification_id;
    $.alertDialog.show();
}

function alertClickFunction(e) {
    if (e.index == 0) {
        var user = Ti.App.Properties.getObject('user', {});
        var data = {
            "data[NotificationsUser][user_id]" : user.id,
            "data[NotificationsUser][carerid]" : user.carerid,
            "data[NotificationsUser][notification_id]" : notificationId,
        };
        if (Alloy.Globals.xhr.isOnline()) {
            $.ai.show();

            Alloy.Globals.xhr.post(Alloy.Globals.notificationDeleteURL, data, function(successResult) {
                Ti.API.info('notifications:::' + JSON.stringify(successResult));
                if (!!successResult.data.success) {
                    Ti.API.info('notifications tableData:::' + tableData.length);
                    tableData = _.reject(tableData, function(row) {
                        Ti.API.info('row.getChildren()[0]. ' + JSON.stringify(row.getChildren()[1]) + ' notificationId  ' + notificationId);
                        return row.getChildren()[1].notification_id == notificationId;
                    });
                    $.notificationsTable.setData([]);
                    $.notificationsTable.setData(tableData);
                    Ti.API.info('notifications tableData:::' + tableData.length);
                    var CFData = Ti.App.Properties.getObject("CFData", {});
                    CFData.Notification = _.reject(CFData.Notification, function(notification) {
                        return notification.id == notificationId;
                    });
                    Ti.App.Properties.setObject("CFData", CFData);
                } else {
                    alert(successResult.data.msg);
                }
                $.ai.hide();
            }, function(errorResult) {
                Ti.API.info(JSON.stringify(errorResult));
                $.ai.hide();
                alert(errorResult);
            });

        } else {
            alert("No Network Connection!");
        }
        $.msgDetailView.width = 0;
        $.msgDetailView.height = 0;
        trx = matrix.scale(0, 0);
        $.msgDetailView.animate({
            transform : trx,
            duration : 200
        });
    }
}

function tableSwipeFunction(e) {
    Ti.API.info('table ' + JSON.stringify(e));
    if (e.direction == 'left' && !focusDeleteButton) {
        animating = true;
        e.row.getChildren()[2].animate({
            right : 0,
            duration : 100
        });
        focusedRow = e.row;
        focusDeleteButton = true;
        setTimeout(function() {
            animating = false;
        }, 250);
    }
};
function tableClickFunction(e) {
    if (focusDeleteButton && !animating) {
        focusDeleteButton = false;
        focusedRow.getChildren()[2].animate({
            right : -86,
            duration : 100
        });
    } else if (!focusDeleteButton && !animating) {
        $.msgLabel.text = e.row.xData.notification;
        notificationId = e.row.xData.id;
        $.msgDetailView.width = Ti.UI.FILL;
        $.msgDetailView.height = 270;
        trx = matrix.scale(1, 1);
        $.msgDetailView.animate({
            transform : trx,
            duration : 200
        });
        var user = Ti.App.Properties.getObject('user', {});
        var data = {
            "data[NotificationsUser][user_id]" : user.id,
            "data[NotificationsUser][carerid]" : user.carerid,
            "data[NotificationsUser][notification_id]" : notificationId,
        };

        Alloy.Globals.xhr.post(Alloy.Globals.notificationReadURL, data, function(successResult) {

        }, function(errorResult) {
            Ti.API.info(JSON.stringify(errorResult));
            // $.ai.hide();
            alert(errorResult);
        });
    }
};
function messageCloseFunction() {
    trx = matrix.scale(0, 0);
    $.msgDetailView.width = 0;
    $.msgDetailView.height = 0;
    $.msgDetailView.animate({
        transform : trx,
        duration : 200
    });
}

function deleteClickFunction() {
    $.alertDialog.show();
}

function notificationFocusFunction(e) {
    $.ai.show();
    Alloy.Globals.dataSynchronize(function(success) {
        tableData = [];
        _.each(Ti.App.Properties.getObject("CFData", {}).Notification, function(_notification, _index) {
            _notification.index = _index;
            tableData.push(Alloy.createController("notificationsCommonView", {
                notification : _notification,
                callback : deleteCallback
            }).getView());
        });
        $.notificationsTable.setData(tableData);
        $.ai.hide();
    });

}

if (OS_IOS)
    require('pulldownrefresh').tablePullHeader($.notificationsTable, notificationFocusFunction);

$.notificationsWin.addEventListener('close', function() {
    $.destroy();
});
