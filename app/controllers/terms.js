var args = arguments[0] || {};
$.termsWin.add(Alloy.createController('navbar', {
    title : ' Terms of Use ',
    isBack : true
}).getView());

$.termsWin.addEventListener('close', function() {
    $.destroy();
});

$.termsWebView.url = args.terms;
