var args = arguments[0] || {};
var moment = require('/alloy/moment');
var day = moment(args.notification.modified);

$.MsgLabel.text = args.notification.notification;
$.MsgMainView.xData = args.notification;
Ti.API.info('args.notification.modified ' + day);
$.dateLabel.text = day.format("DD MMM HH:mm");
//String.formatDate(args.notification.modified)+' '+String.formatTime(args.notification.modified);
$.msgview.notification_id = args.notification.id;

function msgCrossBtnClickFunction(e) {
    args.callback(e.source.notification_id);
}