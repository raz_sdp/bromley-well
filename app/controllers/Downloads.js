var args = arguments[0] || {};
var tableData = [];
$.downloadsWin.add(Alloy.createController('navbar', {
    title : 'Downloads'
}).getView());
function downloadFocusFunction(e) {
    $.ai.show();
    Alloy.Globals.dataSynchronize(function(success) {
        tableData = [];
        _.each(Ti.App.Properties.getObject("CFData", {}).Document, function(doc, _index) {
            doc.index = _index;
            doc.ai = $.ai;
            tableData.push(Alloy.createController('downloadDocCommonView', doc).getView());
        });
        $.downloadsTable.setData(tableData);
        $.documentsCount.text = tableData.length + ' documents available';
        $.ai.hide();
    });
}
$.downloadsWin.addEventListener('close', function() {
    $.destroy();
});