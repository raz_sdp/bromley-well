var args = arguments[0] || {};
var data = Ti.App.Properties.getObject("CFData", {});
Ti.API.info('data     ' + JSON.stringify(data));

$.aboutUsWin.add(Alloy.createController('navbar', {
    title : 'About Us'
}).getView());

data = data.Information[0];
$.phoneLabel.text = data.manager_phone;
$.emailLabel.text = data.contact_email;
//$.readButton.text = data.term_of_use;

var text = "Terms of Use";
var attr = Ti.UI.createAttributedString({
    text : text,
    attributes : [{
        type : Ti.UI.ATTRIBUTE_UNDERLINES_STYLE,
        value : Ti.UI.ATTRIBUTE_UNDERLINE_STYLE_SINGLE, // Ignored by Android only displays a single line
        range : [text.indexOf('Terms of Use'), ('Terms of Use').length]
    }]
});

$.readButton.attributedString = attr;

function contactUsButtonClickFunction() {
    $.dialog.show();
};
function readButtonClickFunction() {
    Alloy.Globals.winOpener('terms', false, {
        terms : Alloy.Globals.termsURL
    });
};
function phoneCall() {
    dialogClickFunction({
        index : 0
    });
}

function emailDialog() {
    dialogClickFunction({
        index : 1
    });
}

function dialogClickFunction(e) {
    if (e.index == 0) {
        Ti.Platform.openURL('tel:' + Ti.App.Properties.getObject("CFData", {}).Information[0].manager_phone);
    } else if (e.index == 1) {
        var emailDialog = Ti.UI.createEmailDialog();
        emailDialog.subject = "Subject";
        emailDialog.toRecipients = [data.contact_email];
        emailDialog.bccRecipients = [];
        emailDialog.ccRecipients = [];
        emailDialog.open();
    }
}

$.aboutUsWin.addEventListener('close', function() {
    $.destroy();
});
