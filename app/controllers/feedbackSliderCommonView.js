var args = arguments[0] || {};
$.quesLabel.text = args.question;
$.sliderMainView.xData = args;

for (var i = 0; i < 10; i++) {
    $.numberView.add(Ti.UI.createLabel({
        text : i + 1,
        color : '#505050',
        width : '10%',
        left : (10 * i) + '%',
        font : {
            fontSize : 9
        },
        textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER
    }));
}

function sliderTouchEndFunction(e) {
    e.source.value = Math.round(e.source.value);
}
