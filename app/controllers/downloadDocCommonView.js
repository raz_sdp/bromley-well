var args = arguments[0] || {};
Ti.API.info('arguments  ' + JSON.stringify(args));
$.docLabel.text = args.title;
$.docMainView.xData = args;

$.searchButton.url = args.file || '';
$.searchButton.xTitle = args.title;
if (OS_ANDROID)
    $.downloadIButton.url = args.file || '';

function searchButtonClickFunction(e) {
    if (OS_ANDROID) {
        var fileUrl = e.source.url;
        var stringParts = fileUrl.split('/'),
            f = Ti.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory, stringParts.pop());
        if (f.exists()) {
            fileUrl = f.nativePath;
            try {
                //var f = Ti.Filesystem.getFile('your.pdf');
                Ti.Android.currentActivity.startActivity(Ti.Android.createIntent({
                    action : Ti.Android.ACTION_VIEW,
                    type : 'application/pdf',
                    data : fileUrl
                }));
            } catch (err) {
                var alertDialog = Titanium.UI.createAlertDialog({
                    title : 'No PDF Viewer',
                    message : 'We tried to open a PDF but failed. Do you want to search the marketplace for a PDF viewer?',
                    buttonNames : ['Yes', 'No'],
                    cancel : 1
                });
                alertDialog.show();
                alertDialog.addEventListener('click', function(evt) {
                    if (evt.index == 0) {
                        Ti.Platform.openURL('http://search?q=pdf');
                    }
                });
            }
        } else {
            Ti.Platform.openURL("https://docs.google.com/gview?embedded=true&url=" + Ti.Network.encodeURIComponent(fileUrl));
        }

    } else {
        var fileUrl = e.source.url;
        var stringParts = fileUrl.split('/'),
            f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, stringParts.pop());
        if (f.exists()) {
            fileUrl = f.nativePath;
        }
        Alloy.Globals.winOpener("downloadDetail", false, {
            url : fileUrl,
            title : e.source.xTitle
        });
    }
}

function downloadIButtonClickFunction(e) {
    var isOnline = Alloy.Globals.xhr.isOnline();
    var fileUrl = e.source.url;
    var stringParts = fileUrl.split('/'),
        f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, stringParts.pop());
    if (f.exists()) {
        alert('File already exists');
    } else {
        if (isOnline) {
            args.ai.show();
            Alloy.Globals.xhr.getFile(fileUrl, function(fileResult) {
                f.write(fileResult.data);
                if (f.exists())
                    f.remoteBackup = false;
                alert('File downloaded successfully.');
                args.ai.hide();
            }, function(errorResult) {
                alert(errorResult);
                args.ai.hide();
            }, {}, function(progress) {
                Ti.API.info('progress ' + progress);
            });
        } else {
            alert('No network connection!');
        }
    }
}
