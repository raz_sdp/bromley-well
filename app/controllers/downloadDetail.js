var args = arguments[0] || {};
$.downloadDetailWin.add(Alloy.createController('navbar', {
    title : args.title,
    isBack : true
}).getView());

Ti.API.info('url_file  ' + args.url);
$.downloadDetailWin.title = args.title;
$.docWebView.url = args.url;

$.downloadDetailWin.addEventListener('close', function() {
    $.destroy();
});