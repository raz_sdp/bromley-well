var args = arguments[0] || {};

var data = Ti.App.Properties.getObject("CFData", {});
Ti.API.info('data  ' + JSON.stringify(data));
data = data.Whats_Happening[0];
$.newsWin.add(Alloy.createController('navbar', {
    title : 'What\'s Happening'
}).getView());
if (data.link)
    $.newsWebView.url = data.link || '';
$.newsWin.addEventListener('close', function() {
    $.destroy();
});
