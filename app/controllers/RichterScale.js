var args = arguments[0] || {};
var assesmentData = [];
var index = 0;
var user_id = '';
var assesment_data = [];
var user = Ti.App.Properties.getObject('user', null);
$.selfWin.add(Alloy.createController('navbar', {
    title : 'Young Carers Assessment',
    callback : showAlert,
    isBack : true
}).getView());

getQuestions();
function getQuestions() {
    if (!!Alloy.Globals.xhr.isOnline()) {
        $.ai.show();
        var url = Alloy.Globals.getAssesmentURL;
        Alloy.Globals.xhr.get(url, function(responceData) {
            $.ai.hide();
            Ti.API.info('active ' + JSON.stringify(responceData));
            assesmentData = responceData.data;
            displayQuestion();
        }, function(errResult) {
            $.ai.hide();
        }, {});

    } else {
        Alloy.Globals.isActive_data = {
            success : false,
            msg : "No internet connectivity is available, please try again later."
        };
    }
};
function changeQuestion(e) {
    if (e.direction == 'left') {
        index++;
        if (index >= assesmentData.length)
            index = assesmentData.length - 1;
    }
    displayQuestion();
}

function resetImg() {
    $.firstAnsView.backgroundImage = "/images/self_assess/option1.png";
    $.firstAnsView.children[0].backgroundImage = "/images/self_assess/iconoption1.png";

    $.secondAnsView.backgroundImage = "/images/self_assess/option2.png";
    $.secondAnsView.children[0].backgroundImage = "/images/self_assess/iconoption2.png";

    $.thirdAnsView.backgroundImage = "/images/self_assess/option3.png";
    $.thirdAnsView.children[0].backgroundImage = "/images/self_assess/iconoption3.png";
}

function selectAnswer(e) {
    var option_id;
    Ti.API.info('question ' + e.source.option);
    resetImg();
    if (e.source.option == 1) {
        option_id = 1;
        $.firstAnsView.backgroundImage = "/images/self_assess/option1-active.png";
        $.smileicon.backgroundImage = "/images/self_assess/activeiconoption1.png";
    } else if (e.source.option == 2) {
        option_id = 2;
        $.secondAnsView.backgroundImage = "/images/self_assess/option2-active.png";
        $.wairningicon.backgroundImage = "/images/self_assess/activeiconoption2.png";
    } else if (e.source.option == 3) {
        option_id = 3;
        $.thirdAnsView.backgroundImage = "/images/self_assess/option3-active.png";
        $.dangericon.backgroundImage = "/images/self_assess/activeiconoption3.png";
    }
    var assesment_id = $.headerLabel.data;
    var status = textFilter(assesment_data, assesment_id);
    Ti.API.info(' status ' + status);
    if (status) {
        assesment_data.splice(status, 1);
    }
    assesment_data[parseInt(assesment_id)] = {
        assesment_id : assesment_id,
        option_id : option_id
    };
    //delete assesment_data[null];
    Ti.API.info('assesment_id ' + JSON.stringify(assesment_data));

    assesment_data = _.compact(assesment_data);
    if (assesment_data.length == assesmentData.length) {
        $.submitButton.touchEnabled = true;
        $.submitButton.visible = true;
        $.infoLabel.visible = true;
        $.progressViewBox.touchEnabled = false;
        $.progressViewBox.visible = false;
    } else {
        // $.progressViewBox.touchEnabled = true;
        // $.progressViewBox.visible = true;
        //  $.submitButton.touchEnabled = false;
        //  $.submitButton.visible = false;
        if (e.type === 'click') {
            setTimeout(function() {
                changeQuestion({
                    direction : 'left'
                });
            }, 500);
        }
    }
}

function displayQuestion() {
    resetImg();
    var question = assesmentData[index];
    Ti.API.info('question ' + JSON.stringify(question));
    $.headerLabel.text = (index + 1) + '. ' + question.Assesment.title;
    $.progressbar.width = (100 / assesmentData.length) * (index + 1) + '%';
    $.numberQLabel.text = String.format("Question %d of %d", (index + 1), assesmentData.length);
    $.headerLabel.data = question.Assesment.id;

    var options = question.Option;

    $.firstAnsView.data = options[0].id;
    $.firstAnsLabel.text = options[0].title;

    $.secondAnsView.data = options[1].id;
    $.secondAnsLabel.text = options[1].title;

    $.thirdAnsView.data = options[2].id;
    $.thirdAnsLabel.text = options[2].title;
    var assesment_id = question.Assesment.id || '';
    if (assesment_data.length && ( assesment_id in assesment_data)) {
        var assesment = assesment_data[assesment_id];
        var option_id = assesment.option_id || '';
        selectAnswer({
            source : {
                option : assesment.option_id || ''
            }
        });
    }
}

function submitUpButtonClickFunction() {
    assesment_data = _.compact(assesment_data);
    if (assesment_data.length < assesmentData.length) {
        var dialog = Ti.UI.createAlertDialog({
            message : "Please complete all questions",
            ok : 'Ok',
            title : 'Warning'
        });
        dialog.show();
    } else {
        Ti.API.info('user ' + JSON.stringify(user));
        var data = {
            "user_id" : user.id,
            "assesment_data" : JSON.stringify(assesment_data)
        };
        if (Alloy.Globals.xhr.isOnline()) {
            $.ai.show();
            Alloy.Globals.xhr.post(Alloy.Globals.root + 'users_options/user_option_insert', data, function(successResult) {

                $.ai.hide();

                var dialog = Ti.UI.createAlertDialog({
                    message : successResult.data.msg,
                    ok : 'Ok',
                    title : 'Info'
                });
                dialog.show();
                dialog.addEventListener('click', submitDialogClick);
            }, function(errorResult) {
                Ti.API.info(JSON.stringify(errorResult));
                $.ai.hide();
                var dialog = Ti.UI.createAlertDialog({
                    message : errorResult,
                    ok : 'Ok',
                    title : 'Warning'
                });
                dialog.show();
            });

        } else {
            var dialog = Ti.UI.createAlertDialog({
                message : 'No Network Connection!',
                ok : 'Ok',
                title : 'Bromley Well'
            });
            dialog.show();
        }
    }

}

function submitDialogClick() {
    Alloy.Globals.winOpener("Home", false, {});
}

function showAlert() {
    var dialog = Ti.UI.createAlertDialog({
        cancel : 1,
        buttonNames : ['Yes', 'Cancel'],
        message : 'Your feedback has not been submitted, are you sure you want exit?',
        title : 'Warning'
    });
    dialog.addEventListener('click', function(e) {
        if (e.index === e.source.cancel) {
            Ti.API.info('The cancel button was clicked');
        } else {
            Alloy.Globals.b();
        }
    });
    dialog.show();

}

if (Alloy.Globals.isiPhoneX()) {
    $.buttonView.height = 120;
}
$.selfWin.addEventListener('close', function() {
    $.destroy();
});
var textFilter = function(array, string) {

    Ti.API.info('textFilter.array ' + JSON.stringify(array) + ' string ' + string);
    var data = [];
    for (var i = 0,
        j = array.length; i < j; i++) {
        var phr = array[i];
        Ti.API.info('phr.assesment_id ' + phr.assesment_id + ' string ' + string);
        if (phr.assesment_id == string) {
            return i;
        }

    };
    return false;
};
