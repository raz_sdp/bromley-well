var args = arguments[0] || {};
$.treeWin.add(Alloy.createController('navbar', {
    title : 'Thought Tree'
}).getView());

$.ai.show();

function alertClickFunction(e) {
    if (e.index == 0) {
        if ($.addResponseTextArea.value) {
            var user = Ti.App.Properties.getObject('user', {});
            var data = {
                "data[Thought][user_id]" : user.id,
                "data[Thought][thought]" : $.addResponseTextArea.value,
            };
            if (Alloy.Globals.xhr.isOnline()) {
                $.ai.show();

                Alloy.Globals.xhr.post(Alloy.Globals.submit_thought, data, function(successResult) {
                    Ti.API.info('notifications:::' + JSON.stringify(successResult));
                    var dialog = Ti.UI.createAlertDialog({
                        message : successResult.data.msg,
                        ok : 'Ok',
                        title : 'Info'
                    });
                    dialog.show();
                    $.ai.hide();
                    $.addResponseTextArea.value = '';
                    $.treeWebView.url = "/tree/index3.html";
                }, function(errorResult) {
                    Ti.API.info(JSON.stringify(errorResult));
                    $.ai.hide();
                    alert(errorResult);
                });
            } else {
                alert("No Network Connection!");
            }

        }
    }
}

function submitButtonClickFunction(e) {
    $.alertDialog.show();
};

$.treeWebView.addEventListener('load', function() {
    $.ai.hide();
});

$.treeWin.addEventListener('close', function() {
    $.destroy();
});
